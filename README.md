Sắp tết - Đếm ngược tết 2023
====================

***Đếm ngược tết 2023***

App tích hợp đủ thử kiến thức kì quái, đến tui còn không hiểu sao lại có cái app này trên đời, ~tuy chưa thật sự~ smooth nhưng mà ít crash :>


<a href="https://play.google.com/store/apps/details?id=com.thanh_nguyen.tet_count_down&hl=en&gl=US" target="_blank">
<img src="https://play-lh.googleusercontent.com/pZCxsZZX1TzPlULn4ONQT6J8K5-STyzra-xY3sldAnNZpm7KiOiSDE_ubH_oY_8dQ6A=s360-rw" alt="Get it on Google Play" height="90"/></a>


Tải app nhẹ nhàng tại
=========

<a href="https://play.google.com/store/apps/details?id=com.thanh_nguyen.tet_count_down&hl=en&gl=US">
  <img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" alt="Get it on Google Play" height="90"/>
</a>


Buy me a Coffee
=========

<a href="https://www.buymeacoffee.com/tlife"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=tlife&button_colour=FFDD00&font_colour=000000&font_family=Cookie&outline_colour=000000&coffee_colour=ffffff" /></a>
