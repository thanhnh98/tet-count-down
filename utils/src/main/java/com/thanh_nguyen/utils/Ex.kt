package com.thanh_nguyen.utils

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


fun WTF(msg: String){
    Log.e("WTF", msg)
}

fun Context.showToastMessage(msg: String){
    GlobalScope.launch {
        withContext(Dispatchers.Main){
            Toast.makeText(this@showToastMessage, msg, Toast.LENGTH_SHORT).show()
        }
    }
}