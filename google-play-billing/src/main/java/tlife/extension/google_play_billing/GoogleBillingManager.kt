package tlife.extension.google_play_billing

import android.app.Activity
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.BillingResponseCode.*
import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.collect.ImmutableList
import com.thanh_nguyen.utils.WTF
import com.thanh_nguyen.utils.showToastMessage
import kotlinx.coroutines.Dispatchers

class GoogleBillingManager(activity: Activity) {

    private var activity: Activity? = activity

    private val purchasesUpdatedListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
                for (purchase in purchases) {
                    WTF("purchase: $purchase")
                }
            } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                // Handle an error caused by a user cancelling the purchase flow.
                WTF("Cancel purchase")
            } else {
                // Handle any other error codes.
                WTF("failed purchase")
            }
        }

    private var billingClient = BillingClient.newBuilder(activity!!)
        .setListener(purchasesUpdatedListener)
        .enablePendingPurchases()
        .build()

    suspend fun connectGooglePlay(
        onSetupFinished: () -> Unit,
        onSetupFailed: () -> Unit
    ){
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                WTF("result: ${billingResult.responseCode}")
                when (billingResult.responseCode) {
                    OK -> {
                        onSetupFinished.invoke()
                    }
                    SERVICE_TIMEOUT -> {

                    }
                    FEATURE_NOT_SUPPORTED -> {

                    }
                    SERVICE_DISCONNECTED -> {
                        activity?.showToastMessage("Mất kết nối dịch vụ")
                    }
                    USER_CANCELED -> {

                    }
                    SERVICE_UNAVAILABLE -> {

                    }
                    BILLING_UNAVAILABLE -> {

                    }
                    ITEM_UNAVAILABLE -> {
                        activity?.showToastMessage("Sản phẩm tạm thời không khả dụng")
                    }
                    DEVELOPER_ERROR -> {

                    }
                    ERROR -> {

                    }
                    ITEM_ALREADY_OWNED -> {
                        activity?.showToastMessage("Đã mua sản phẩm này rồi")
                    }
                    ITEM_NOT_OWNED -> {

                    }
                }
            }
            override fun onBillingServiceDisconnected() {
                onSetupFailed.invoke()
                activity?.showToastMessage("Kết nối dịch vụ thất bại, thử lại sau.")
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        })
    }

    suspend fun processPurchases(productId: String, type: BillingProductType) {
        val productList =  ImmutableList.of(
            QueryProductDetailsParams.Product.newBuilder()
                .setProductId(productId)
                .setProductType(type.type)
                .build())

        val params =
            QueryProductDetailsParams.newBuilder()
                .setProductList(productList)

        kotlinx.coroutines.withContext(Dispatchers.IO) {
            val productDetails = billingClient.queryProductDetails(params.build())
            productDetails.productDetailsList?.apply {
                if (this.isEmpty())
                    return@withContext
                launch(first(), "null")
            }
        }
    }

    private fun launch(
        productDetails: ProductDetails,
        offerToken: String
    ){
        val productDetailsParamsList = listOf(
            BillingFlowParams.ProductDetailsParams.newBuilder()
                // retrieve a value for "productDetails" by calling queryProductDetailsAsync()
                .setProductDetails(productDetails)
                // to get an offer token, call ProductDetails.subscriptionOfferDetails()
                // for a list of offers that are available to the user
//                .setOfferToken(offerToken)
                .build()
        )

        val billingFlowParams = BillingFlowParams.newBuilder()
            .setProductDetailsParamsList(productDetailsParamsList)
            .build()
        activity?.apply {
            val billingResult = billingClient.launchBillingFlow(this, billingFlowParams)
        }
    }
}