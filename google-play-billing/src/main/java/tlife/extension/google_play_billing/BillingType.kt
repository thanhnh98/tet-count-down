package tlife.extension.google_play_billing

import com.android.billingclient.api.BillingClient

enum class BillingProductType(val type: String) {
    IN_APP(BillingClient.ProductType.INAPP),
    SUBS(BillingClient.ProductType.SUBS)
}