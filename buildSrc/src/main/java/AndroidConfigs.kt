object AndroidConfigs {
    const val compileSdkVersion = 33
    const val buildToolsVersion = "30.0.2"

    const val applicationId = "com.thanh_nguyen.tet_count_down"
    const val minSdkVersion = 24
    const val targetSdkVersion = 33

    const val versionCode = 338
    const val versionName = "3.3.8"
}