object Modules {
    const val facebook = ":facebook"
    const val google = ":google"
    const val youtube = ":youtube"
    const val image_compressor = ":image_compressor"
    const val utils = ":utils"
    const val compressor = ":compressor"
    const val google_play_billing = ":google-play-billing"
}