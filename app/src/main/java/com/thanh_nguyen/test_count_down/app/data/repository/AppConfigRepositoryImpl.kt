package com.thanh_nguyen.test_count_down.app.data.repository

import com.thanh_nguyen.test_count_down.app.data.data_source.remote.AppConfigRemoteDataSource
import com.thanh_nguyen.test_count_down.app.domain.repositories.AppConfigRepository
import com.thanh_nguyen.test_count_down.app.model.app_config.AppConfigModel
import com.thanh_nguyen.test_count_down.app.model.response.Result
import kotlinx.coroutines.flow.Flow

class AppConfigRepositoryImpl(
    private val appConfigRemoteDataSource: AppConfigRemoteDataSource
): AppConfigRepository {
    override fun getAppConfig(): Flow<Result<AppConfigModel>> {
         return appConfigRemoteDataSource.getAppConfig()
    }
}