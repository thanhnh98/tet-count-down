package com.thanh_nguyen.test_count_down.app.presentation.dialog.setting

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.thanh_nguyen.test_count_down.App
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.data.data_source.local.AppPreferences
import com.thanh_nguyen.test_count_down.app.data.data_source.local.AppSharedPreferences
import com.thanh_nguyen.test_count_down.app.model.response.onResultReceived
import com.thanh_nguyen.test_count_down.app.presentation.dialog.FullHeightBottomSheet
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.MainActivity
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.about.AboutViewModel
import com.thanh_nguyen.test_count_down.common.AdsManager
import com.thanh_nguyen.test_count_down.common.MusicState
import com.thanh_nguyen.test_count_down.common.SoundManager
import com.thanh_nguyen.test_count_down.databinding.BottomSheetSettingBinding
import com.thanh_nguyen.test_count_down.external.KeyStore
import com.thanh_nguyen.test_count_down.external.firebase.AppAnalytics
import com.thanh_nguyen.test_count_down.utils.compress
import com.thanh_nguyen.test_count_down.utils.loadImageUri
import com.thanh_nguyen.test_count_down.utils.onClick
import com.thanh_nguyen.utils.showToastMessage
import kodeinViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SettingBottomSheet: FullHeightBottomSheet<BottomSheetSettingBinding>() {
    private val adsManager: AdsManager by lazy {
        AdsManager(App.getInstance())
    }
    private var listener: SettingChangeListener? = null
    private var isSelectedDefault = true
    private var selectedBitmap: Uri? = null
    val viewModel: AboutViewModel by kodeinViewModel()

    private val soundManager: SoundManager by lazy {
        (activity as MainActivity).soundManager
    }

    // Registers a photo picker activity launcher in single-select mode.
    private val pickMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
        if (uri != null) {
            cacheImage(uri)
        } else {
            Toast.makeText(App.getInstance(), "Hình ảnh ko hợp lệ, chọn ảnh khác bạn nhé <3 ", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun updateThumbCustomTheme(uri: Uri) {
        AppAnalytics.trackChangeBackground()
        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                binding.icAdditional.visibility = View.GONE
                loadImageUri(uri, binding.imgThemeCustom)
                selectedBitmap = uri
                selectCustomTheme()
            }
            AppPreferences.saveCustomTheme(uri)
        }
    }
    private fun cacheImage(uri: Uri) {
        lifecycleScope.launch {
            withContext(Dispatchers.IO){
                val compressedFile = compress(
                    uri
                )
                if (compressedFile == null){
                    activity?.showToastMessage("Có lỗi xảy ra trong quá trình tải lên")
                }
                else {
                    withContext(Dispatchers.Main){
                        updateThumbCustomTheme(compressedFile.toUri())
                    }
                }
            }
        }
    }

    override fun inflateLayout(): Int = R.layout.bottom_sheet_setting

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adsManager.prepareAds()
        AppAnalytics.trackOpenSetting()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imgClose.onClick {
            dismiss()
        }
        bindCurrentSetting()
        bindListenerEvent()
        setUpAds()
        setupObserver()
    }

    private fun bindListenerEvent() {
        binding.swDailyNoti.setOnCheckedChangeListener { buttonView, isChecked ->
            listener?.onDailyNotiStatusChanged(isChecked)
        }

        binding.swMusic.setOnCheckedChangeListener { buttonView, isChecked ->
            changeStatusSound(isChecked)
        }

        binding.swPinnedNoti.setOnCheckedChangeListener { buttonView, isChecked ->
            listener?.onPinnedNotiChanged(isChecked)
        }
        binding.imgTheme.onClick {
            lifecycleScope.launch {
                selectDefaultTheme()
            }
        }
        binding.imgThemeCustom.onClick {
            if (selectedBitmap == null){
                pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
            }
            else {
                lifecycleScope.launch {
                    selectCustomTheme()
                }
            }
        }

        binding.tvChangeCustomTheme.onClick {
            pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }
    }

    private fun changeStatusSound(isMutedSound: Boolean) {
        if (isMutedSound) {
            soundManager.notifyChangeState(MusicState.Play())
        }
        else{
            soundManager.notifyChangeState(MusicState.Pause())
        }
    }

    private fun selectCustomTheme() {
        isSelectedDefault = false
        binding.imgCheckedThemeCustom.visibility = View.VISIBLE
        binding.imgCheckedTheme.visibility = View.GONE
        binding.icAdditional.visibility = View.GONE
        listener?.onBackgroundChangeRequest(selectedBitmap)
        lifecycleScope.launch {
            AppPreferences.saveCustomTheme(selectedBitmap)
        }
        adsManager.show(
            requireActivity()
        )
    }

    private fun selectDefaultTheme() {
        isSelectedDefault = true
        binding.imgCheckedThemeCustom.visibility = View.GONE
        binding.imgCheckedTheme.visibility = View.VISIBLE
        listener?.onBackgroundChangeRequest(null)
        lifecycleScope.launch {
            AppPreferences.saveCustomTheme(null)
        }
    }

    private fun bindCurrentSetting() {
        lifecycleScope.launch {
            AppSharedPreferences.isEnabledCountDownNoti.collect {
                val isHasNotificationPinned = it ?: false
                binding.swPinnedNoti.isChecked = isHasNotificationPinned
                this.cancel()
            }
        }

        lifecycleScope.launch {
            withContext(Dispatchers.IO){
                val isDailyNotiEnable = AppPreferences.getDailyNotiEnable()
                val musicName = AppPreferences.getCurrentBackgroundMusic()?.name ?: "N/A"
                val isPlaying = soundManager.getBackgroundMusic()?.isPlaying ?: false
                selectedBitmap = AppPreferences.getSavedCustomTheme()
                withContext(Dispatchers.Main){
                    with(binding) {
                        tvMusicName.text = musicName
                        swMusic.isChecked = isPlaying
                        swDailyNoti.isChecked = isDailyNotiEnable
                        selectedBitmap = AppPreferences.getSavedCustomTheme()
                        if (selectedBitmap != null){
                            selectCustomTheme()
                            loadImageUri(selectedBitmap!!,  binding.imgThemeCustom)
                        }
                    }
                }
            }
        }
        lifecycleScope.launch {
            val isMusicPlaying = !AppPreferences.isBackgroundMuted
            binding.swMusic.isChecked = isMusicPlaying
        }
    }

    fun setSettingListener(listener: SettingChangeListener): SettingBottomSheet{
        this.listener = listener
        return this
    }

    @SuppressLint("MissingPermission")
    private fun setupObserver() {
        lifecycleScope.launchWhenCreated {
            viewModel.adsInfo.collect {
                it.onResultReceived(
                    onLoading = {

                    },
                    onError = {

                    },
                    onSuccess = {
                        binding.lnlAds.addView(
                            createAdsView(KeyStore.getAdsBannerAbout()).apply {
                                loadAd(AdRequest.Builder().build().apply {
                                    Log.e("CHECKING IS ADS TEST DEVICE", "${isTestDevice(context)}")
                                })
                            }
                        )
                    }
                )
            }
        }
    }

    private fun createAdsView(adsId: String): AdView {
        return AdView(App.getInstance()).apply {
            adSize = AdSize.BANNER
            adUnitId = adsId
        }
    }

    private fun setUpAds() {
        viewModel.getAdsInfo()
    }

    override fun onDestroyView() {
        binding.lnlAds.removeAllViews()
        super.onDestroyView()
    }
}
