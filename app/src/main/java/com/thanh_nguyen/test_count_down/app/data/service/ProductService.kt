package com.thanh_nguyen.test_count_down.app.data.service

import com.thanh_nguyen.test_count_down.app.model.ProductInAppModel
import com.thanh_nguyen.test_count_down.app.model.response.BaseResponse
import retrofit2.Response
import retrofit2.http.GET

interface ProductService {
    @GET("main/in-app-products")
    suspend fun getListProducts(): Response<BaseResponse<List<ProductInAppModel>>>
}