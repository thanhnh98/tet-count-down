package com.thanh_nguyen.test_count_down.app.domain.repositories

import com.thanh_nguyen.test_count_down.app.model.app_config.AppConfigModel
import com.thanh_nguyen.test_count_down.app.model.response.Result
import kotlinx.coroutines.flow.Flow

interface AppConfigRepository {
    fun getAppConfig(): Flow<Result<AppConfigModel>>
}