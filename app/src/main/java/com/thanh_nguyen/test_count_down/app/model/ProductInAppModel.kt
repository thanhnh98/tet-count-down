package com.thanh_nguyen.test_count_down.app.model

data class ProductInAppModel(
    val id: Int,
    val product_id: String,
    val name: String,
    val type: String,
    val price: String,
    val active: Boolean
): BaseModel()