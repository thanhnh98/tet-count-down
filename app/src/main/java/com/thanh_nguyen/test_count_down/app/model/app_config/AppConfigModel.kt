package com.thanh_nguyen.test_count_down.app.model.app_config

import com.thanh_nguyen.test_count_down.app.model.BaseModel

data class AppConfigModel(
    val version: AppVersionModel
): BaseModel()