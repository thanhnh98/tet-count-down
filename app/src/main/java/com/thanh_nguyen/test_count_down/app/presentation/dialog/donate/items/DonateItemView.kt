package com.thanh_nguyen.test_count_down.app.presentation.dialog.donate.items

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.model.ProductInAppModel
import com.thanh_nguyen.test_count_down.common.base.adapter.BindingRecycleViewItem
import com.thanh_nguyen.test_count_down.databinding.ItemDonateBinding
import com.thanh_nguyen.test_count_down.utils.inflateView
import com.thanh_nguyen.test_count_down.utils.onClick

class DonateItemView(
    private val product: ProductInAppModel,
    private val onItemClick: (ProductInAppModel) -> Unit
): BindingRecycleViewItem<ItemDonateBinding, DonateItemVH>() {
    override fun inflateViewHolder(parent: ViewGroup): DonateItemVH {
        return DonateItemVH(
            inflateView(parent, R.layout.item_donate)
        )
    }

    override fun bindModel(binding: ItemDonateBinding?, viewHolder: DonateItemVH) {
        binding?.root?.onClick {
            onItemClick.invoke(product)
        }

        binding?.tvProductName?.text = product.name
        binding?.tvPrice?.text = product.price
    }
}

class DonateItemVH(view: View):  RecyclerView.ViewHolder(view)