package com.thanh_nguyen.test_count_down.app.presentation.dialog.donate

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.model.ProductInAppModel
import com.thanh_nguyen.test_count_down.app.presentation.dialog.FullHeightBottomSheet
import com.thanh_nguyen.test_count_down.app.presentation.dialog.donate.items.DonateItemView
import com.thanh_nguyen.test_count_down.common.base.adapter.RecycleViewItem
import com.thanh_nguyen.test_count_down.common.base.adapter.RecyclerManager
import com.thanh_nguyen.test_count_down.databinding.BottomSheetDonateBinding
import com.thanh_nguyen.test_count_down.external.firebase.AppAnalytics
import com.thanh_nguyen.test_count_down.utils.onClick
import com.thanh_nguyen.test_count_down.utils.showToastMessage
import kodeinViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import tlife.extension.google_play_billing.BillingProductType
import tlife.extension.google_play_billing.GoogleBillingManager

class DonateBottomSheet: FullHeightBottomSheet<BottomSheetDonateBinding>() {
    private var recyclerManager = RecyclerManager<Any>()
    private var gridLayoutManager = GridLayoutManager(context, 1)
    private val viewModel: DonateViewModel by kodeinViewModel()

    override fun inflateLayout(): Int = R.layout.bottom_sheet_donate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppAnalytics.trackOpenDonate()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClusters()
        with(binding.recyclerView){
            layoutManager = gridLayoutManager
            adapter = recyclerManager.adapter
        }
        binding.imgClose.onClick {
            dismiss()
        }
        setupObserver()
        viewModel.getProducts()
    }

    private fun setupObserver() {
        lifecycleScope.launch {
            viewModel.product.collectLatest {
                showListProducts(it.filter { item ->
                    item.active
                })
            }
        }
    }

    private fun initClusters() {
        recyclerManager.addCluster(DonateItemView::class.java)
    }

    private fun showListProducts(data: List<ProductInAppModel>){
        val listItems: MutableList<RecycleViewItem<*>> = ArrayList()

        data.onEach {
            listItems.add(
                DonateItemView(it){ product ->
                    AppAnalytics.trackOpenDonateItem(product.name)
                    purchase(product.product_id, BillingProductType.IN_APP)
                }
            )
        }
        recyclerManager.replace(DonateItemView::class.java, listItems)
    }

    private fun purchase(productId: String, type: BillingProductType){
        lifecycleScope.launch {
            GoogleBillingManager(activity?:return@launch).run {
                connectGooglePlay(
                    onSetupFailed = {
                        activity?.showToastMessage("Kết nối thất bại")
                    },
                    onSetupFinished = {
                        lifecycleScope.launch {
                            processPurchases(productId, type)
                        }
                    }
                )
            }
        }
    }
}