package com.thanh_nguyen.test_count_down.app.presentation.dialog.donate

import androidx.lifecycle.viewModelScope
import com.thanh_nguyen.test_count_down.app.domain.usecases.ProductUseCase
import com.thanh_nguyen.test_count_down.app.model.ProductInAppModel
import com.thanh_nguyen.test_count_down.common.base.mvvm.viewmodel.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class DonateViewModel(
    private val productUseCase: ProductUseCase
): BaseViewModel() {
    private var _product: MutableStateFlow<List<ProductInAppModel>> = MutableStateFlow(emptyList())
    val product: StateFlow<List<ProductInAppModel>> get() = _product

    fun getProducts(){
        viewModelScope.launch {
            productUseCase.getProducts().collectLatest {
                _product.emit(it.data?.data?: emptyList())
            }
        }
    }
}