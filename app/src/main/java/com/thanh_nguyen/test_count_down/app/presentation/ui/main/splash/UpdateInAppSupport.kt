package com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash

import android.app.Activity
import android.util.Log
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.AppUpdateOptions
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability

class UpdateInAppSupport: IUpdateInAppSupport {
    companion object {
        const val TRACKING_UPDATE_REQUEST_CODE = 0x001001
    }

    private lateinit var activity: Activity
    private lateinit var appUpdateManager: AppUpdateManager
    private lateinit var callback: ITrackingUpdateCallback
    private var isForceUpdate: Boolean = false


    val listener = InstallStateUpdatedListener { state ->
        when (state.installStatus()) {
            InstallStatus.DOWNLOADING -> {
                val bytesDownloaded = state.bytesDownloaded()
                val totalBytesToDownload = state.totalBytesToDownload()
                Log.d("InstallStatus.DOWNLOADING:","$bytesDownloaded/$totalBytesToDownload")
            }

            InstallStatus.DOWNLOADED -> {
                completeUpdate()
            }

            InstallStatus.INSTALLED -> {
                unregisterListener()
            }

            else -> {
                //Do nothing
            }
        }
    }

    override fun registerInAppUpdate(
        activity: Activity,
        isForceUpdate: Boolean,
        callback: ITrackingUpdateCallback
    ) {
        this.activity = activity
        this.callback = callback
        this.isForceUpdate = isForceUpdate
        setupForceUpdate(activity)
    }

    override fun onUpdateResult(resultCode: Int) {
        when(resultCode){
            Activity.RESULT_OK -> {
                callback.onUpdateSuccess(isForceUpdate)
            }
            else -> {
                callback.onUpdateFailed(resultCode, isForceUpdate)
            }
        }
    }

    private fun setupForceUpdate(activity: Activity) {
        if (!::appUpdateManager.isInitialized)
            appUpdateManager = AppUpdateManagerFactory.create(activity)
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            val isAvailable = appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
            if (isAvailable) {
                registerListener()
                requestUpdateApp(
                    appUpdateInfo,
                    isForceUpdate = isForceUpdate
                )
            }
            else {
                callback.onUpdateUnavailable(isForceUpdate)
            }
        }
    }

    private fun requestUpdateApp(
        appUpdateInfo: AppUpdateInfo,
        isForceUpdate: Boolean
    ) {
        appUpdateManager.startUpdateFlowForResult(
            appUpdateInfo,
            activity,
            AppUpdateOptions.newBuilder(AppUpdateType.IMMEDIATE)
                .setAllowAssetPackDeletion(true)
                .build(),
            TRACKING_UPDATE_REQUEST_CODE
        )
    }

    override fun completeUpdate(){
        appUpdateManager.completeUpdate()
    }

    override fun checkUpdateInProgress(
        activity: Activity
    ) {
        if (!::appUpdateManager.isInitialized)
            appUpdateManager = AppUpdateManagerFactory.create(activity)

        appUpdateManager
            .appUpdateInfo
            .addOnSuccessListener { appUpdateInfo ->
                if (appUpdateInfo.updateAvailability()
                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                ) {
                    // If an in-app update is already running, resume the update.
                    appUpdateManager.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.IMMEDIATE,
                        activity,
                        TRACKING_UPDATE_REQUEST_CODE
                    )
                }
                if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED){
                    completeUpdate()
                }
            }
    }


    private fun registerListener(){
        appUpdateManager.registerListener(listener)
    }

    private fun unregisterListener(){
        appUpdateManager.unregisterListener(listener)
    }
}