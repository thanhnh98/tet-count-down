package com.thanh_nguyen.test_count_down.app

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash.GetStartedScreen
import com.thanh_nguyen.test_count_down.common.Constants
import com.thanh_nguyen.test_count_down.sys_communicate.service.WidgetUpdateService
import com.thanh_nguyen.test_count_down.utils.formatTwoNumber
import com.thanh_nguyen.test_count_down.utils.getSecondsUntilDate

/**
 * Implementation of App Widget functionality.
 */
class CountDownWidget : AppWidgetProvider() {
    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)

        val appWidgetManager = AppWidgetManager.getInstance(
            context
        )
        val thisWidget = ComponentName(
            context ?: return,
            CountDownWidget::class.java
        )
        val appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget)
        if (appWidgetIds != null && appWidgetIds.isNotEmpty()) {
            onUpdate(context, appWidgetManager, appWidgetIds)
        }
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        context.sendBroadcast(Intent(context, CountDownWidget::class.java).apply {
            action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        })
        try {
            context.startService(Intent(context, WidgetUpdateService::class.java))
        }
        catch (e: Exception){

        }
    }

    override fun onDisabled(context: Context) {
        try {
            context.stopService(Intent(context, WidgetUpdateService::class.java))
        }
        catch (e: Exception){

        }
    }
}

internal fun updateAppWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int
) {
    val minuteSec = 60
    val hourSec = minuteSec * 60
    val daySec = hourSec * 24
    val totalSeconds = getSecondsUntilDate(Constants.EventDate.LUNAR_NEW_YEAR)
    val days = totalSeconds /  daySec
    val hours = (totalSeconds - days * daySec) / hourSec
    val minutes = (totalSeconds - days * daySec - hours * hourSec) / minuteSec
    val seconds = totalSeconds - days * daySec - hours * hourSec - minutes * minuteSec

    // Construct the RemoteViews object
    val views = RemoteViews(context.packageName, R.layout.count_down_widget)
    views.setTextViewText(R.id.tv_day, "${days.formatTwoNumber()}\nNgày")
    views.setTextViewText(R.id.tv_hour, "${hours.formatTwoNumber()}\nGiờ")
    views.setTextViewText(R.id.tv_minute, "${minutes.formatTwoNumber()}\nPhút")
    views.setTextViewText(R.id.tv_second, "${seconds.formatTwoNumber()}\nGiây")
    views.setOnClickPendingIntent(
        R.id.layout_root,
        PendingIntent.getActivity(
            context,
            0,
            Intent(
                context, GetStartedScreen::class.java
            ),
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
    )
    // Instruct the widget manager to update the widget
    appWidgetManager.updateAppWidget(appWidgetId, views)
}