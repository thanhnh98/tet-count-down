package com.thanh_nguyen.test_count_down.app.data.data_source.remote

import com.thanh_nguyen.test_count_down.app.data.service.ProductService

class ProductRemoteDataSource(
    private val api: ProductService
): BaseRemoteDataSource() {
    fun getProducts() = getResult {
        api.getListProducts()
    }
}