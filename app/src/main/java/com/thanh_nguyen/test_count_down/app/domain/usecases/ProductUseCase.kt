package com.thanh_nguyen.test_count_down.app.domain.usecases

import com.thanh_nguyen.test_count_down.app.domain.repositories.ProductRepository

class ProductUseCase(
    private val repository: ProductRepository
) {
    fun getProducts() = repository.getProducts()
}