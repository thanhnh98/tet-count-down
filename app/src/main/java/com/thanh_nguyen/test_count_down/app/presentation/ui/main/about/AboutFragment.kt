package com.thanh_nguyen.test_count_down.app.presentation.ui.main.about

import android.annotation.SuppressLint
import android.content.res.loader.ResourcesProvider
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.thanh_nguyen.test_count_down.App
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.data.data_source.local.AppPreferences
import com.thanh_nguyen.test_count_down.app.model.AboutHeaderDataModel
import com.thanh_nguyen.test_count_down.app.model.AboutItemDataModel
import com.thanh_nguyen.test_count_down.app.model.response.onResultReceived
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.MainActivity
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.about.item.content.AboutViewItem
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.about.item.header.AboutHeaderViewItem
import com.thanh_nguyen.test_count_down.common.Constants
import com.thanh_nguyen.test_count_down.common.MusicState
import com.thanh_nguyen.test_count_down.common.SoundManager
import com.thanh_nguyen.test_count_down.common.base.mvvm.fragment.BaseCollectionFragmentMVVM
import com.thanh_nguyen.test_count_down.databinding.FragmentAboutBinding
import com.thanh_nguyen.test_count_down.external.KeyStore
import com.thanh_nguyen.test_count_down.utils.observeLiveDataChanged
import com.thanh_nguyen.test_count_down.utils.onClick
import kodeinViewModel
import kotlinx.coroutines.flow.collect

class AboutFragment: BaseCollectionFragmentMVVM<FragmentAboutBinding, AboutViewModel>() {

    override val viewModel: AboutViewModel by kodeinViewModel()

    private val soundManager: SoundManager by lazy {
        (activity as MainActivity).soundManager
    }

    override fun inflateLayout(): Int = R.layout.fragment_about

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreatedX(view: View, savedInstanceState: Bundle?) {
        super.onViewCreatedX(view, savedInstanceState)
        setupEventUI()
        setupObserver()
        setUpAds()
        setupData()
        binding.vHome.onClick {
            (activity as MainActivity).navigateToTab(0, true)
        }

        binding.tvMusicName.onClick {
            (activity as MainActivity).navigateToTab(2, true)
        }

        binding.icGotoMusic.onClick {
            (activity as MainActivity).navigateToTab(2, true)
        }

        binding.flImgSoundContainer.onClick {
            if (AppPreferences.isBackgroundMuted) {
                soundManager.notifyChangeState(MusicState.Play())
            }
            else{
                soundManager.notifyChangeState(MusicState.Pause())
            }
        }

        binding.tvMusicName.text = AppPreferences.getCurrentBackgroundMusic()?.title?:Constants.DEFAULT_MUSIC_NAME
    }


    private fun setupEventUI() {
//        binding.imgTheme?.setImageDrawable(
//            ContextCompat.getDrawable(
//                activity ?: return,
//                Constants.CURRENT_EVENT.backgroundImage
//            )
//        )
//
//        binding.layoutContainer.background = ContextCompat.getDrawable(
//            activity ?: return,
//            Constants.CURRENT_EVENT.backgroundWishBox
//        )
    }

    private fun bindVolumeData(isMuted: Boolean){
        if (isMuted){
            binding.imgSound.setImageResource(R.drawable.ic_volume_off)
        }
        else{
            binding.imgSound.setImageResource(R.drawable.ic_volume_on)
        }
    }

    private fun setupObserver() {2333
        lifecycleScope.launchWhenCreated {
            viewModel.adsInfo.collect {
                it.onResultReceived(
                    onLoading = {

                    },
                    onError = {

                    },
                    onSuccess = {
                        binding.lnlAds.addView(
                            createAdsView(KeyStore.getAdsBannerAbout()).apply {
                                loadAd(AdRequest.Builder().build().apply {
                                    Log.e("CHECKING IS ADS TEST DEVICE", "${isTestDevice(context)}")
                                })
                            }
                        )
                    }
                )
            }
        }

        observeLiveDataChanged((activity as MainActivity).soundManager.musicStateChanged){
            binding.tvMusicName.requestFocus()
            when(it){
                is MusicState.Play -> {
                    bindVolumeData(false)
                }

                is MusicState.Pause -> {
                    bindVolumeData(true)
                }

                is MusicState.UpdateMusic -> {
                    binding.tvMusicName.text = it.localMusic.title
                }

                is MusicState.Stop -> {

                }
            }
        }
    }

    private fun createAdsView(adsId: String): AdView{
        return AdView(activity).apply {
            adSize = AdSize.BANNER
            adUnitId = adsId
        }
    }

    private fun setUpAds() {
        viewModel.getAdsInfo()
    }

    private fun setupData() {
        showHeaderItem(
            AboutHeaderDataModel(
                title = getString(R.string.about_header_title),
                content = getString(R.string.about_header_content)
            )
        )
        showListSectionData(listOf(
            AboutItemDataModel(
                title = getString(R.string.about_block_1_title),
                content = getString(R.string.about_block_1_content),
                imgDrawable = AppCompatResources.getDrawable(App.getInstance(), R.drawable.lich_nghi_tet),
                imgSource = "Lịch tết 2023"
            )
        ))
    }

    override fun initClusters() {
        addCluster(AboutHeaderViewItem::class)
        addCluster(AboutViewItem::class)
    }

    private fun showHeaderItem(headerData: AboutHeaderDataModel){
        recyclerManager.replace(
            AboutHeaderViewItem::class,
            AboutHeaderViewItem(headerData)
        )
    }

    private fun showListSectionData(items: List<AboutItemDataModel>){
        recyclerManager.replace(
            AboutViewItem::class,
            createListAboutItem(items)
        )
    }

    private fun createListAboutItem(items: List<AboutItemDataModel>): List<AboutViewItem> {
        return items.map { aboutItemData ->
             AboutViewItem(
                 aboutItemData
            )
        }
    }

    override fun onRefresh() {
        super.onRefresh()
        hideLoading()
    }

    override fun onResume() {
        super.onResume()
        binding.tvMusicName.requestFocus()
    }


    override fun onDestroyView() {
        binding.lnlAds.removeAllViews()
        super.onDestroyView()
    }
}