package com.thanh_nguyen.test_count_down.app.data.data_source.local

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.Uri
import androidx.core.net.toUri
import androidx.datastore.preferences.core.stringPreferencesKey
import com.google.gson.GsonBuilder
import com.thanh_nguyen.test_count_down.App
import com.thanh_nguyen.test_count_down.app.model.HomeEventModel
import com.thanh_nguyen.test_count_down.app.model.LocalMusicModel
import com.thanh_nguyen.test_count_down.utils.decodeBase64
import com.thanh_nguyen.test_count_down.utils.encodeToBase64
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object AppPreferences {
    val preferences: SharedPreferences = App.getInstance().getSharedPreferences("SapTetPreferences", Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = preferences.edit()

    private val IS_MUTED = "IS_MUTED"
    private val BACKGROUND_MUSIC = "BACKGROUND_MUSIC"
    private val LATEST_VERSION_CODE = "LATEST_VERSION_CODE"
    private val LATEST_EVENT = "LATEST_EVENT"
    private val ENABLE_DAILY_NOTI = "ENABLE_DAILY_NOTI"
    private val THEME_CUSTOM_SELECTED_URI = "THEME_CUSTOM_SELECTED_URI"

    var isBackgroundMuted: Boolean
        set(isBackgroundMuted){
            editor.putBoolean(IS_MUTED, isBackgroundMuted).commit()
        }
        get() = preferences.getBoolean(IS_MUTED, false)

    fun saveCurrentBackgroundMusic(music: LocalMusicModel){
        putObject(music, BACKGROUND_MUSIC)
    }

    fun getCurrentBackgroundMusic(): LocalMusicModel? = getObject(BACKGROUND_MUSIC)


    fun saveLatestVersionIgnored(versionCode: Int){
        editor.putInt(LATEST_VERSION_CODE, versionCode).commit()
    }

    fun getLatestEvent(): HomeEventModel? = getObject(LATEST_EVENT) as? HomeEventModel

    fun saveLatestEvent(event: HomeEventModel){
        putObject(event, LATEST_EVENT)
    }

    fun saveDailyNotiEnable(isEnable: Boolean){
        editor.putBoolean(ENABLE_DAILY_NOTI, isEnable).commit()
    }

    fun getDailyNotiEnable(): Boolean{
        return preferences.getBoolean(ENABLE_DAILY_NOTI, true)
    }

    fun saveCustomTheme(uri: Uri?){
        editor.putString(THEME_CUSTOM_SELECTED_URI, uri?.toString()).commit()
    }

    fun getSavedCustomTheme(): Uri? {
        val uri =
            try {
                preferences.getString(THEME_CUSTOM_SELECTED_URI, null)?.toUri()
            } catch (e: Exception) {
                null
            }
        return uri
    }

    fun getLatestVersionIgnored(): Int = preferences.getInt(LATEST_VERSION_CODE, -1)


    private fun putObject(`object`: Any?, key: String) {
        //Convert object to JSON String.
        try {
            val jsonString = GsonBuilder().create().toJson(`object`)
            //Save that String in SharedPreferences
            preferences.edit().putString(key, jsonString).apply()
        }catch (e:Throwable){}
    }

    fun clearPreference(key:String){
        preferences.edit().remove(key).apply()
    }

    inline fun <reified T> getObject(key: String): T? {
        //We read JSON String which was saved.
        val value = preferences.getString(key, null)
        //JSON String was found which means object can be read.
        //We convert this JSON String to model object. Parameter "c" (of
        //type Class < T >" is used to cast.
        try {
            return GsonBuilder().create().fromJson(value, T::class.java)
        }catch (e:Throwable){
            return null
        }
    }

}