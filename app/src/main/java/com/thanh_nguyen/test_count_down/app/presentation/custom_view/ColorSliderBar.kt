package com.thanh_nguyen.test_count_down.app.presentation.custom_view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.thanh_nguyen.test_count_down.databinding.ViewColorSliderBinding
import java.lang.String
import kotlin.math.max

class ColorSliderBar: ConstraintLayout {
    private lateinit var binding: ViewColorSliderBinding

    constructor(context: Context) : super(context) {
        setup(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setup(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        setup(context)
    }

    private fun setup(context: Context) {
        binding = ViewColorSliderBinding.inflate(LayoutInflater.from(context), this, true)
        setupGesture()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupGesture() {
        binding.root.setOnTouchListener { _, event ->
            val xValue = when {
                event.x < binding.root.x -> {
                    binding.root.x
                }
                event.x > binding.root.x + binding.root.width -> {
                    binding.root.x + binding.root.width
                }
                else -> {
                    event.x
                }
            }
            val intValue = Int.MAX_VALUE
            val percent = event.x / binding.root.width.toFloat()

            val hexColor: kotlin.String = String.format("#%06X", 0xFFFFFF and (intValue * percent).toInt())

            binding.vCursor.setBackgroundColor(Color.parseColor(hexColor))

            binding.vCursor.x = max(xValue, binding.root.x)

            true
        }
    }

}