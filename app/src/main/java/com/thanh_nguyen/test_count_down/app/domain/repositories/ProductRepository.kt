package com.thanh_nguyen.test_count_down.app.domain.repositories

import com.thanh_nguyen.test_count_down.app.model.ProductInAppModel
import com.thanh_nguyen.test_count_down.app.model.response.BaseResponse
import com.thanh_nguyen.test_count_down.app.model.response.*
import kotlinx.coroutines.flow.Flow

interface ProductRepository {
    fun getProducts(): Flow<Result<BaseResponse<List<ProductInAppModel>>>>
}