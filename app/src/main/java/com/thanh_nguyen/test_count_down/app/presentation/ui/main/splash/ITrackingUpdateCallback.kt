package com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash

interface ITrackingUpdateCallback {
    fun onUpdateSuccess(isForceUpdate: Boolean)
    fun onUpdateFailed(resultCode: Int, isForceUpdate: Boolean)
    /**
     * when @param isForceUpdate == true, SHOULD SHOW POPUP TO NOTIFY IT'S A NEW UPDATE READY
     */
    fun onUpdateUnavailable(isForceUpdate: Boolean)
}