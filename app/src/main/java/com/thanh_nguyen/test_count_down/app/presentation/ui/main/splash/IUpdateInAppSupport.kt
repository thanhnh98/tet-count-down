package com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash

import android.app.Activity
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash.ITrackingUpdateCallback

interface IUpdateInAppSupport {
    fun registerInAppUpdate(activity: Activity, isForceUpdate: Boolean, callback: ITrackingUpdateCallback)

    /**
     * trigger this function on onActivityForResult in parent Activity
     */
    fun onUpdateResult(resultCode: Int)

    /**
     * completeUpdate must call when newer version had been downloaded
     */
    fun completeUpdate()

    /**
     * should be checked onCreate or onResume
     */
    fun checkUpdateInProgress(activity: Activity)
}