package com.thanh_nguyen.test_count_down.app.data.data_source.remote

import com.thanh_nguyen.test_count_down.app.data.service.AppConfigService

class AppConfigRemoteDataSource(
    private val appConfigService: AppConfigService
): BaseRemoteDataSource() {
    fun getAppConfig() = getResult {
        appConfigService.getAppConfig()
    }
}