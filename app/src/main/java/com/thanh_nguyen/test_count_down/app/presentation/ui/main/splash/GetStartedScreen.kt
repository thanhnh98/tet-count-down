/*
 * Created by Thanh Nguyen on 11/23/21, 4:01 PM
 */

package com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import com.thanh_nguyen.test_count_down.BuildConfig
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.data.data_source.local.AppPreferences
import com.thanh_nguyen.test_count_down.app.data.data_source.local.AppSharedPreferences
import com.thanh_nguyen.test_count_down.app.model.response.onResultReceived
import com.thanh_nguyen.test_count_down.app.presentation.dialog.notify_update_dialog.NotifyUpdateDialog
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.MainActivity
import com.thanh_nguyen.test_count_down.common.Constants
import com.thanh_nguyen.test_count_down.common.SoundManager
import com.thanh_nguyen.test_count_down.common.base.mvvm.activity.BaseActivityMVVM
import com.thanh_nguyen.test_count_down.databinding.ActivityGetStartedBinding
import com.thanh_nguyen.test_count_down.utils.isTetOnGoing
import com.thanh_nguyen.utils.WTF
import kodeinViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class GetStartedScreen: BaseActivityMVVM<ActivityGetStartedBinding, GetStartedVM>(),
    IUpdateInAppSupport by UpdateInAppSupport(),
    ITrackingUpdateCallback
{
    private val soundManager: SoundManager by instance()
    private var isGoneToMain = false

    override val viewModel: GetStartedVM by kodeinViewModel()

    override fun inflateLayout(): Int = R.layout.activity_get_started

    override fun onCreate(savedInstanceState: Bundle?) {
        // Handle the splash screen transition.
        installSplashScreen()
        super.onCreate(savedInstanceState)
        enableFullScreen()
        soundManager.playFireworkSound()
        playViewAnimation()
        if (BuildConfig.DEBUG){
            determineStartedScreen()
        }
        else
            setupFlowResult()
        viewModel.getAppConfig()
    }

    private fun setupFlowResult() {
        lifecycleScope.launch {
            viewModel.appConfig.collect {
                it.onResultReceived(
                    onLoading = {
                    },
                    onSuccess = { res ->
                        res.data?.version?.apply {
                            val cachedVersion = AppPreferences.getLatestVersionIgnored()
                            AppPreferences.saveLatestVersionIgnored(lastVersionCode)

                            if (cachedVersion != -1 && cachedVersion == lastVersionCode){
                                determineStartedScreen()
                                return@onResultReceived
                            }

                            if (lastVersionCode <= BuildConfig.VERSION_CODE){
                                determineStartedScreen()
                                return@onResultReceived
                            }

                            NotifyUpdateDialog()
                                .apply {
                                    isCancelable = false
                                }
                                .setTitle("Thông báo cập nhật")
                                .setDescription("Có bản cập nhật mới rồi, cập nhật ngay nhé!!")
                                .setPositiveText("Cập nhật ngay")
                                .setNegativeText("Để sau")
                                .onPrimaryClick {
                                    registerInAppUpdate(
                                        this@GetStartedScreen,
                                        isForceUpdate = true,
                                        this@GetStartedScreen
                                    )
                                }
                                .onNegativeClick {
                                    determineStartedScreen()
                                }
                                .show(
                                    supportFragmentManager,
                                    "onFlexibleUpdateDownloaded"
                                )
                        }
                    },
                    onError = {
                        determineStartedScreen()
                    }
                )
            }
        }
    }

    private fun playViewAnimation() {
        binding.imgHpny.animate()
            .setDuration(1000)
            .alpha(1f)
            .setListener(object: Animator.AnimatorListener{
                override fun onAnimationStart(p0: Animator) {

                }

                override fun onAnimationEnd(p0: Animator) {
                    binding.imgFlowerTop.alpha = 1f
                }

                override fun onAnimationCancel(p0: Animator) {
                }

                override fun onAnimationRepeat(p0: Animator) {
                }

            })
            .start()

        binding.imgFlowerTop.animate()
            .setDuration(1000)
            .setListener(object: Animator.AnimatorListener{
                override fun onAnimationStart(p0: Animator) {

                }

                override fun onAnimationEnd(p0: Animator) {
                    binding.imgFlowerTop.alpha = 1f
                }

                override fun onAnimationCancel(p0: Animator) {
                }

                override fun onAnimationRepeat(p0: Animator) {
                }

            })
            .alpha(1f)
            .start()

        binding.imgFlowerBottom.animate()
            .setDuration(1000)
            .setListener(object: Animator.AnimatorListener{
                override fun onAnimationStart(p0: Animator) {

                }

                override fun onAnimationEnd(p0: Animator) {
                    binding.imgFlowerBottom.alpha = 1f
                }

                override fun onAnimationCancel(p0: Animator) {
                }

                override fun onAnimationRepeat(p0: Animator) {
                }

            })
            .alpha(1f)
            .start()
    }

    private fun determineStartedScreen(){
        lifecycleScope.launch {
            delay(2000)
            AppSharedPreferences.setIsVisited(true)
            if (isTetOnGoing())
                onTetGoing()
            else
                goToMain()
        }
    }

    private fun onTetGoing(){
        soundManager.playBackgroundSound()
        with(binding){
            val phraseMap =  Constants.Phrases.listWishes.random()
            val pharse1 = phraseMap.first.split(" ")
            val pharse2 = phraseMap.second.split(" ")
            val texts: MutableList<String> = ArrayList()
            texts.run {
                addAll(pharse1)
                addAll(pharse2)
            }
            val listView = listOf(
                tv1,
                tv2,
                tv3,
                tv4,
                tv5,
                tv6,
                tv7,
                tv8
            )

            listView.forEachIndexed { index, textView ->
                textView.text = texts[index]
            }

            animateView(imgHpny, alpha = 0.5f, scaleX = 0.5f, scaleY = 0.5f){
                imgHpny.alpha = 0.5f
                imgHpny.scaleX = 0.5f
                imgHpny.scaleY = 0.5f
                animateView(tv1){
                    animateView(tv2){
                        animateView(tv3){
                            animateView(tv4){
                                animateView(tv5){
                                    animateView(tv6){
                                        animateView(tv7){
                                            animateView(tv8){

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun goToMain(){
        startActivity(Intent(this@GetStartedScreen, MainActivity::class.java))
        isGoneToMain = true
        finish()
    }

    override fun onResume() {
        super.onResume()
        soundManager.playFireworkSound()
    }

    override fun onPause() {
        super.onPause()
        soundManager.pauseFireworkSound()
        if (isTetOnGoing())
            soundManager.pauseBackgroundSound()
    }

    override fun onDestroy() {
        super.onDestroy()
        soundManager.stopFireworkSound()
        if (isTetOnGoing())
            soundManager.stopBackgroundSound()
    }

    private fun animateView(view1: View,
                            alpha: Float = 1f,
                            scaleX: Float = 1f,
                            scaleY: Float = 1f,
                            callback: () -> Unit){
        view1.animate()
            .setDuration(500L)
            .setListener(object: Animator.AnimatorListener{
                override fun onAnimationStart(p0: Animator) {

                }

                override fun onAnimationEnd(p0: Animator) {
                    view1.alpha = 1f
                    callback.invoke()
                }

                override fun onAnimationCancel(p0: Animator) {
                }

                override fun onAnimationRepeat(p0: Animator) {
                }

            })
            .alpha(alpha)
            .scaleX(scaleX)
            .scaleY(scaleY)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UpdateInAppSupport.TRACKING_UPDATE_REQUEST_CODE){
            onUpdateResult(resultCode)
        }
    }

    override fun onUpdateSuccess(isForceUpdate: Boolean) {
        determineStartedScreen()
    }

    override fun onUpdateFailed(resultCode: Int, isForceUpdate: Boolean) {
        determineStartedScreen()
    }

    override fun onUpdateUnavailable(isForceUpdate: Boolean) {
        determineStartedScreen()
    }
}