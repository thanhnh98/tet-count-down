package com.thanh_nguyen.test_count_down.app.presentation.ui.main.home

import android.animation.Animator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.icu.util.ChineseCalendar
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.view.children
import androidx.lifecycle.lifecycleScope
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.thanh_nguyen.test_count_down.App
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.data.data_source.local.AppPreferences
import com.thanh_nguyen.test_count_down.app.data.data_source.local.AppSharedPreferences
import com.thanh_nguyen.test_count_down.app.presentation.dialog.donate.DonateBottomSheet
import com.thanh_nguyen.test_count_down.app.presentation.dialog.setting.SettingBottomSheet
import com.thanh_nguyen.test_count_down.app.presentation.dialog.setting.SettingChangeListener
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash.GetStartedScreen
import com.thanh_nguyen.test_count_down.app.presentation.ui.main.MainActivity
import com.thanh_nguyen.test_count_down.common.Constants.Companion.CURRENT_EVENT
import com.thanh_nguyen.test_count_down.common.MusicState
import com.thanh_nguyen.test_count_down.common.SoundManager
import com.thanh_nguyen.test_count_down.common.base.mvvm.fragment.BaseFragmentMVVM
import com.thanh_nguyen.test_count_down.databinding.FragmentHomeBinding
import com.thanh_nguyen.test_count_down.sys_communicate.service.CountDownForegroundService
import com.thanh_nguyen.test_count_down.utils.*
import com.thanh_nguyen.utils.WTF
import com.thanh_nguyen.utils.showToastMessage
import kodeinViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import tlife.extension.google_play_billing.GoogleBillingManager
import java.util.*

class HomeFragment: BaseFragmentMVVM<FragmentHomeBinding, HomeViewModel>() {
    override val viewModel: HomeViewModel by kodeinViewModel()
    private val soundManager: SoundManager by lazy {
        (activity as MainActivity).soundManager
    }

    private var isMutedSound: Boolean = false

    override fun inflateLayout(): Int = R.layout.fragment_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setOnTouchListener { _, event ->
            val x = event.x
            val y = event.y
            when(event.action and MotionEvent.ACTION_MASK){
                MotionEvent.ACTION_DOWN -> {
                    binding.containerFireworks.addView(
                        createLottieView(x, y)
                    )
                }
            }
            false
        }
        setupEventUI()
        viewModel.startCountDown()
        viewModel.getWishes()
        setupBackgroundMusic()
        setup()
        setupOnClick()
    }

    private fun setupEventUI() {
        binding.tvWish.background = getDrawable(activity?:return, CURRENT_EVENT.backgroundWishBox)
        lifecycleScope.launch {

            if (AppPreferences.getSavedCustomTheme() != null){
                withContext(Dispatchers.Main){
                    try {
                        loadImageUri(AppPreferences.getSavedCustomTheme()!!, binding.imgTheme?:return@withContext )
                    }
                    catch (e: Exception){
                        e.printStackTrace()
                    }
                }
            }
            else {
                withContext(Dispatchers.Main){
                    binding.imgTheme?.setImageDrawable(getDrawable(requireContext(), CURRENT_EVENT.backgroundImage))
                }
            }
        }
    }

    private fun setupOnClick() {
        binding.flImgPinContainer.onClick {
            changeStatusCountDownNotification()
        }

        binding.flImgSoundContainer.onClick {
            changeStatusSound()
        }
        binding.tvMusicName.onClick {
            (activity as MainActivity).navigateToTab(2, true)
        }
        binding.icGotoMusic.onClick {
            (activity as MainActivity).navigateToTab(2, true)
        }
        binding.icMenu?.onClick {
            showWindowPopup(it)
        }
    }

    private fun showWindowPopup(viewCall: View) {
        val context = activity?:return

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupLayout = inflater.inflate(R.layout.popup_menu, null)
        val popupWidth = resources.getDimensionPixelSize(R.dimen.popup_width)
        val popupHeight = resources.getDimensionPixelSize(R.dimen.popup_height)
        val popupWindow = PopupWindow(
            popupLayout,
            popupWidth,
            popupHeight,
            true
        )

        popupLayout.findViewById<TextView>(R.id.tv_other_apps).onClick {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/dev?id=5540559479839330036")))
            popupWindow.dismiss()
        }

        popupLayout.findViewById<TextView>(R.id.tv_setting).onClick {
            openSettingScreen()
            popupWindow.dismiss()
        }

        popupLayout.findViewById<TextView>(R.id.tv_share).onClick {
            shareApp()
            popupWindow.dismiss()
        }


        popupLayout.findViewById<TextView>(R.id.tv_lucky_money).onClick {
            openDonateScreen()
//            activity?.showToastMessage("Tính năng đang tạm bảo trì, thử lại sau nhé ^^")
            popupWindow.dismiss()
        }

        //show popup menu
        val values = IntArray(2)
        viewCall.getLocationInWindow(values)
        val positionOfIcon = values[1]
        val height = getScreenHeight() * 2 / 3
        if (positionOfIcon > height) {
            // when parent view in the bottom of the screen show popup up
            val offsetY = resources.getDimensionPixelSize(R.dimen._200dp)
            popupWindow.showAsDropDown(viewCall, 0, -offsetY, Gravity.END)
        } else {
            // when parent view in the bottom of the screen show popup down
            popupWindow.showAsDropDown(
                viewCall,
                0,
                convertDpToPixel(8F).toInt(),
                Gravity.END
            )
        }
    }

    private fun openDonateScreen() {
        DonateBottomSheet()
            .show(parentFragmentManager, "Donate")
    }

    private fun openSettingScreen() {
        SettingBottomSheet()
            .setSettingListener(object : SettingChangeListener {
                override fun onDailyNotiStatusChanged(isEnable: Boolean) {
                    AppPreferences.saveDailyNotiEnable(isEnable)
                }

                override fun onPinnedNotiChanged(isEnable: Boolean) {
                    changeStatusCountDownNotification()
                }

                override fun onBackgroundChangeRequest(uri: Uri?) {
                    if (uri != null){
                        loadImageUri(uri, binding.imgTheme?:return)
                    }
                    else {
                        binding.imgTheme?.setImageDrawable(getDrawable(activity?:return, CURRENT_EVENT.backgroundImage))
                    }
                }
            })
            .show(parentFragmentManager, "Setting")
    }

    private fun shareApp(){
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "Cùng đợi đến tết thôiii...\nhttps://saptet.page.link/invite")
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }
    private fun changeStatusSound() {
        if (isMutedSound) {
            soundManager.notifyChangeState(MusicState.Play())
        }
        else{
            soundManager.notifyChangeState(MusicState.Pause())
        }
    }

    private fun changeStatusCountDownNotification(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            lifecycleScope.launch {
                AppSharedPreferences
                    .isEnabledCountDownNoti
                    .collect{ currentState ->
                        if (currentState != true) {
                            activity?.startForegroundService(
                                Intent(
                                    activity,
                                    CountDownForegroundService::class.java
                                )
                            )
                        } else {
                            activity?.stopService(
                                Intent(
                                    activity,
                                    CountDownForegroundService::class.java
                                )
                            )
                        }
                    }
            }.cancel() //cancel after finished
        }
        else {
            activity?.showToastMessage("Tính năng không hỗ trợ trên phiên bản android hiện tại")

        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setup() {
        observeLiveDataChanged(viewModel.homeData){ data ->
            data.date.apply{
                if (isTetOnGoing()){
                    startActivity(Intent(activity?:return@apply, GetStartedScreen::class.java))
                }
                else if (isForeground){
                    with(binding){
                        tvDay.text = day.formatTwoNumber()
                        tvHour.text = hour.formatTwoNumber()
                        tvMinute.text = minute.formatTwoNumber()
                        tvSecond.text = second.formatTwoNumber()
                    }
                }
            }
        }

        observeLiveDataChanged(viewModel.wishesData){ wishData ->
            wishData.data?.apply {
                binding.tvWish.text = this.random()
                binding.tvWish.animate()
                    .alpha(1f).duration = 500
            }
        }

        lifecycleScope.launch {
            AppSharedPreferences.isEnabledCountDownNoti.collect { isEnable ->
                if (isEnable != true){
                    binding.imgPin.setImageDrawable(App.getResources().getDrawable(R.drawable.ic_unpin, null))
                }
                else
                    binding.imgPin.setImageDrawable(App.getResources().getDrawable(R.drawable.ic_pin, null))
            }
        }

        buildCalendar()
    }

    private fun buildCalendar() {
        lifecycleScope.launchWhenCreated {
            val calendar = Calendar.getInstance()
            val date = Date(calendar.timeInMillis)

            doOnIO{
                val dayOfWeek = getDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK))
                val dayOfMonth  = calendar.get(Calendar.DAY_OF_MONTH)
                val month = (calendar.get(Calendar.MONTH)) % 12  + 1
                val year = calendar.get(Calendar.YEAR)
                launch {
                    doOnMain {
                        binding.tvCalendar?.text = "$dayOfWeek, $dayOfMonth/$month/$year"
                        binding.tvCalendar?.fadeInAppearance()
                    }
                }
            }

            doOnIO {
                val chineseCalendar = ChineseCalendar(date)
                val chineseMonth = (chineseCalendar.get(Calendar.MONTH)) % 12  + 1
                val chineseDay = chineseCalendar.get(Calendar.DAY_OF_MONTH)
                launch {
                    doOnMain {
                        val date = "($chineseDay/$chineseMonth âm lịch)"
                        binding.tvChineseCalendar?.visibility = View.VISIBLE
                        binding.tvChineseCalendar?.text = date
                        binding.tvChineseCalendar?.fadeInAppearance()
                    }
                }
            }
        }

    }

    private fun setupBackgroundMusic() {
        observeLiveDataChanged(soundManager.musicStateChanged) {
            binding.tvMusicName?.requestFocus()
            when(it){
                is MusicState.Play -> {
                    isMutedSound = false
                    updateSoundUI()
                    binding.ltMusic?.playAnimation()
                }

                is MusicState.Pause -> {
                    isMutedSound = true
                    updateSoundUI()
                    binding.ltMusic?.pauseAnimation()
                }

                is MusicState.UpdateMusic -> {
                    binding.tvMusicName?.text = it.localMusic.title
                    isMutedSound = !it.requestPlay
                    updateSoundUI()
                }

                is MusicState.Stop -> {

                }
            }
        }
    }

    private fun updateSoundUI(){
        if (isMutedSound){
            binding.imgSound.setImageDrawable(
                getDrawable(
                    activity ?: return,
                    R.drawable.ic_volume_off
                )
            )
            binding.ltMusic?.pauseAnimation()
        }
        else
            binding.imgSound.setImageDrawable(
                getDrawable(
                    activity ?: return,
                    R.drawable.ic_volume_on
                )
            )
        binding.ltMusic?.playAnimation()

    }

    private fun createLottieView(x: Float, y: Float): LottieAnimationView{
        val lottieFile =  LottieAnimationView(activity).apply {
            val size = convertDpToPixel(160f).toInt()
            layoutParams = ViewGroup.LayoutParams(size, size)
            setX(x - size/2)
            setY(y - size/2)
            setAnimation("fireworks_click_action.json")
            repeat(1){}
            playAnimation()
        }
        lottieFile.addAnimatorListener(object: Animator.AnimatorListener{
            override fun onAnimationStart(p0: Animator) {
            }

            override fun onAnimationEnd(p0: Animator) {
                removeClickActionAnim(lottieFile)
            }

            override fun onAnimationCancel(p0: Animator) {
            }

            override fun onAnimationRepeat(p0: Animator) {
            }

        })
        return lottieFile
    }

    private fun removeClickActionAnim(view: View){
        try {
            if (binding.containerFireworks.children.contains(view))
                binding.containerFireworks.removeView(view)
        }catch (e: Exception){

        }
    }

    override fun onResume() {
        super.onResume()
        binding.tvMusicName.requestFocus()
    }

    override fun onDestroyView() {
        binding.containerFireworks.removeAllViews()
        super.onDestroyView()
    }
}