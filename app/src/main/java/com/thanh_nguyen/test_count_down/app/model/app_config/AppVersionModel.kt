package com.thanh_nguyen.test_count_down.app.model.app_config

import com.thanh_nguyen.test_count_down.app.model.BaseModel

data class AppVersionModel(
    val lastVersionCode: Int,
    val forceResetData: Int,//equal version code
): BaseModel()
