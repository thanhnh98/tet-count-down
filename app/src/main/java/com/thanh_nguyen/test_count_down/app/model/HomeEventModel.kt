package com.thanh_nguyen.test_count_down.app.model

import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.utils.isTetOnGoing
import com.thanh_nguyen.utils.WTF
import java.util.*

enum class HomeEventModel(
    val themeColor: Int,
    val backgroundWishBox: Int,
    val backgroundImage: Int,
    val backgroundMusic: EventMusicModel
){
    TET(
        R.color.colorPrimary,
        R.drawable.bg_wish_container,
        R.drawable.theme_tet,
        EventMusicModel(
            R.raw.background_music,
            "Hopeful Freedom",
            "Asher Fulero",
        )
    ),
    MIDDLE_AUG(
        R.color.colorPrimary,
        R.drawable.bg_wish_container,
        R.drawable.theme_tet,
        EventMusicModel(
            R.raw.background_music,
            "Hopeful Freedom",
            "Asher Fulero",
        )
    ),
    NOEL(
        R.color.colorPrimary,
        R.drawable.bg_wish_container,
        R.drawable.theme_noel,
        EventMusicModel(
            R.raw.music_noel,
            "We Wish You A Merry Christmas",
            "Crazy Frog",
        )
    ),
}

data class EventMusicModel(
    val musicId: Int,
    val musicName: String,
    val musicSingerName: String
)

fun getEventByDate(): HomeEventModel {
    val calendar = Calendar.getInstance()
    val date = Date(calendar.timeInMillis)
    val month = (calendar.get(Calendar.MONTH)) % 12 + 1
    WTF("month: $month")
    return when {
        month >= 10 -> HomeEventModel.NOEL
        month >= 8 -> HomeEventModel.MIDDLE_AUG
        else -> HomeEventModel.TET
    }
}