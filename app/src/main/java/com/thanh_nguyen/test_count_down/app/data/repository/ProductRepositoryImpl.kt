package com.thanh_nguyen.test_count_down.app.data.repository

import com.thanh_nguyen.test_count_down.app.data.data_source.remote.ProductRemoteDataSource
import com.thanh_nguyen.test_count_down.app.domain.repositories.ProductRepository
import com.thanh_nguyen.test_count_down.app.model.ProductInAppModel
import com.thanh_nguyen.test_count_down.app.model.response.BaseResponse
import com.thanh_nguyen.test_count_down.app.model.response.Result
import kotlinx.coroutines.flow.Flow

class ProductRepositoryImpl(
    private val productDataSource: ProductRemoteDataSource
): ProductRepository {
    override fun getProducts(): Flow<Result<BaseResponse<List<ProductInAppModel>>>> {
        return productDataSource.getProducts()
    }
}