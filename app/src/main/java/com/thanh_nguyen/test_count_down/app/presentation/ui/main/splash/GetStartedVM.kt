package com.thanh_nguyen.test_count_down.app.presentation.ui.main.splash

import androidx.lifecycle.viewModelScope
import com.thanh_nguyen.test_count_down.app.domain.usecases.AppConfigUseCase
import com.thanh_nguyen.test_count_down.app.model.app_config.AppConfigModel
import com.thanh_nguyen.test_count_down.common.base.mvvm.viewmodel.BaseViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import com.thanh_nguyen.test_count_down.app.model.response.Result
import com.thanh_nguyen.utils.WTF

class GetStartedVM(
    private val appConfigUseCase: AppConfigUseCase
): BaseViewModel() {
//    init {
//        getAppConfig()
//    }
    private val _appConfig = MutableSharedFlow<Result<AppConfigModel>>()
    val appConfig: Flow<Result<AppConfigModel>> get() =  _appConfig

    fun getAppConfig(){
        viewModelScope.launch(ioContext) {
            appConfigUseCase.getAppConfig().collect {
                _appConfig.emit(it)
            }
        }
    }
}