package com.thanh_nguyen.test_count_down.app.presentation.dialog.setting

import android.net.Uri

interface SettingChangeListener {
    fun onDailyNotiStatusChanged(isEnable: Boolean)
    fun onPinnedNotiChanged(isEnable: Boolean)
    fun onBackgroundChangeRequest(uri: Uri?)
}