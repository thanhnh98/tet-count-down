package com.thanh_nguyen.test_count_down.app.domain.usecases

import com.thanh_nguyen.test_count_down.app.domain.repositories.AppConfigRepository

class AppConfigUseCase(
    private val appConfigRepository: AppConfigRepository
) {
    fun getAppConfig() = appConfigRepository.getAppConfig()
}