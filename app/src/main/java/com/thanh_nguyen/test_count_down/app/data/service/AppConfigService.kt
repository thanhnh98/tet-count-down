package com.thanh_nguyen.test_count_down.app.data.service

import com.thanh_nguyen.test_count_down.app.model.app_config.AppConfigModel
import retrofit2.Response
import retrofit2.http.GET

interface AppConfigService {
    @GET("main/app-config")
    suspend fun getAppConfig(): Response<AppConfigModel>
}