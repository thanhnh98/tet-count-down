package com.thanh_nguyen.test_count_down.app.presentation.dialog.notify_update_dialog

import android.os.Bundle
import android.view.View
import com.thanh_nguyen.test_count_down.R
import com.thanh_nguyen.test_count_down.app.presentation.dialog.BaseDialog
import com.thanh_nguyen.test_count_down.databinding.DialogNotifyUpdateAppBinding
import com.thanh_nguyen.test_count_down.utils.onClick

class NotifyUpdateDialog: BaseDialog<DialogNotifyUpdateAppBinding>() {
    override fun inflateLayout(): Int  = R.layout.dialog_notify_update_app

    private var onPrimaryClick: (() -> Unit)? = null
    private var onNegativeClick: (() -> Unit)? = null
    private var positiveText: String? = null
    private var negativeText: String? = null
    private var desctiption: String? = null
    private var title: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvNegative.onClick {
            dismiss()
            onNegativeClick?.invoke()
        }

        binding.tvPositive.onClick {
            onPrimaryClick?.invoke()
        }

        positiveText?.apply {
            binding.tvPositive.text = this
        }

        negativeText?.apply {
            binding.tvNegative.text = this
        }

        title?.apply {
            binding.tvTitle.text = this
        }

        desctiption?.apply {
            binding.tvDescription.text = this
        }

        if (isCancelable){
            binding.bgDialog.onClick {
                dismiss()
            }
        }
    }

    fun setPositiveText(text: String): NotifyUpdateDialog{
        this.positiveText = text
        return this
    }

    fun setNegativeText(text: String): NotifyUpdateDialog {
        this.negativeText = text
        return this
    }

    fun setTitle(text: String): NotifyUpdateDialog{
        this.title = text
        return this
    }

    fun setDescription(text: String): NotifyUpdateDialog {
        this.desctiption = text
        return this
    }

    fun onPrimaryClick(func: () -> Unit): NotifyUpdateDialog{
        this.onPrimaryClick = func
        return this
    }

    fun onNegativeClick(func: () -> Unit): NotifyUpdateDialog{
        this.onNegativeClick = func
        return this
    }
}