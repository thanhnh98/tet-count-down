package com.thanh_nguyen.test_count_down.sys_communicate.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import com.thanh_nguyen.test_count_down.external.firebase.AppAnalytics
import com.thanh_nguyen.test_count_down.sys_communicate.receiver.event.UpdateCountDownReceiverEvent
import com.thanh_nguyen.test_count_down.sys_communicate.service.CountDownForegroundService

class UpdateCountDownServiceReceiver: BroadcastReceiver() {
    companion object {
        const val RECEIVER_EVENT = "ReceiverEvent"
    }
    override fun onReceive(context: Context?, intent: Intent?) {
        val event = intent?.extras?.getSerializable(RECEIVER_EVENT) as UpdateCountDownReceiverEvent?
        when (event) {
            UpdateCountDownReceiverEvent.CLOSE_COUNT_DOWN -> {
                AppAnalytics.trackCloseCountDownNotification()
                context?.stopService(Intent(context, CountDownForegroundService::class.java))
            }
            UpdateCountDownReceiverEvent.PINNED_COUNT_DOWN -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    AppAnalytics.trackPinnedNotification()
                    context?.startForegroundService(Intent(context, CountDownForegroundService::class.java))
                }
            }
            else -> {}
        }
    }

}