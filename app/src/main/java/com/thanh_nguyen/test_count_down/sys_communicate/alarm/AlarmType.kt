package com.thanh_nguyen.test_count_down.sys_communicate.alarm


enum class AlarmType {
    DAILY_REMIND,
    WIDGET_TICKING
}