package com.thanh_nguyen.test_count_down.sys_communicate.receiver.event

enum class UpdateCountDownReceiverEvent {
    CLOSE_COUNT_DOWN,
    PINNED_COUNT_DOWN,
}