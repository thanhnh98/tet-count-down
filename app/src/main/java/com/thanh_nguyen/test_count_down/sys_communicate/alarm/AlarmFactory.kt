package com.thanh_nguyen.test_count_down.sys_communicate.alarm

import android.app.PendingIntent
import android.content.Context

class AlarmFactory {
   companion object {
       fun createAlarm(context: Context, alarmType: AlarmType): PendingIntent? {
           return when(alarmType){
               AlarmType.DAILY_REMIND -> {
                   createAlarmDailyTetRemaining(context)
               }
               AlarmType.WIDGET_TICKING -> {
                   createAlarmForWidgetTicking(context)
               }
           }
       }
   }
}