package com.thanh_nguyen.test_count_down.sys_communicate.receiver.event

enum class WidgetTickEvent {
    START_WIDGET,
    TICK_WIDGET
}