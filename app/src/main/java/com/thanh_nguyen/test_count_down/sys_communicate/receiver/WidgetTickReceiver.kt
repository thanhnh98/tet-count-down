package com.thanh_nguyen.test_count_down.sys_communicate.receiver

import android.appwidget.AppWidgetManager
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import com.thanh_nguyen.test_count_down.app.CountDownWidget
import com.thanh_nguyen.test_count_down.sys_communicate.receiver.event.WidgetTickEvent
import com.thanh_nguyen.test_count_down.sys_communicate.service.WidgetUpdateService

class WidgetTickReceiver: BroadcastReceiver() {
    companion object {
        const val WIDGET_TICK_EVENT = "WIDGET_TICK_EVENT"
        const val ACTION_WIDGET_UPDATE = "WIDGET_TICK_RECEIVER"

    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val event = intent?.extras?.getSerializable(WIDGET_TICK_EVENT) as WidgetTickEvent?
        when (event) {
            WidgetTickEvent.START_WIDGET -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context?.startForegroundService(
                        Intent(
                            context,
                            WidgetUpdateService::class.java
                        )
                    )
                }
                else {
                    context?.startService(
                        Intent(
                            context,
                            WidgetUpdateService::class.java
                        )
                    )
                }
            }
            WidgetTickEvent.TICK_WIDGET -> {
                updateCountDownWidget(context?:return)
            }
            null -> {

            }
        }
    }

    private fun updateCountDownWidget(context: Context) {
        val intent = Intent(context, CountDownWidget::class.java)
        intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        val ids: IntArray =
            AppWidgetManager.getInstance(context)
                .getAppWidgetIds(ComponentName(context, CountDownWidget::class.java))
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        context.sendBroadcast(intent)
    }

}