package com.thanh_nguyen.test_count_down.sys_communicate.alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import com.thanh_nguyen.test_count_down.App
import com.thanh_nguyen.test_count_down.sys_communicate.receiver.AlarmReceiver
import com.thanh_nguyen.test_count_down.sys_communicate.receiver.WidgetTickReceiver
import com.thanh_nguyen.test_count_down.sys_communicate.receiver.event.WidgetTickEvent
import java.util.*

internal val alarmManager: AlarmManager = App.getInstance().getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
const val RC_DAILY_NOTI_ALARM = 999
const val RC_WIDGET_TICKING = 998

fun setAlarmRemindAfterInterval(
    context: Context,
    alarmType: AlarmType,
    interval: Long = AlarmManager.INTERVAL_DAY,
    sourceType: Int = AlarmManager.RTC_WAKEUP
){
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.HOUR_OF_DAY, 7)
    calendar.set(Calendar.MINUTE, 0)
    val timeMillis = calendar.timeInMillis
    alarmManager.setRepeating(
        sourceType,
        timeMillis,
        interval,
        AlarmFactory.createAlarm(
            context,
            alarmType
        )
    )
}

internal fun createAlarmDailyTetRemaining(context: Context): PendingIntent?{
    val intentAlarm = Intent(context, AlarmReceiver::class.java)
    return PendingIntent.getBroadcast(
        context,
        RC_DAILY_NOTI_ALARM,
        intentAlarm,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
    )
}


internal fun createAlarmForWidgetTicking(context: Context): PendingIntent{
    val intentAlarm = Intent(context, WidgetTickReceiver::class.java).apply {
        putExtras(
            bundleOf(
                WidgetTickReceiver.WIDGET_TICK_EVENT to WidgetTickEvent.START_WIDGET
            )
        )
    }
    return PendingIntent.getBroadcast(
        context,
        RC_WIDGET_TICKING,
        intentAlarm,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
    )
}

fun closeAlarm(context: Context, type: AlarmType){
    val pendingIntent = when (type){
        AlarmType.DAILY_REMIND -> {
            createAlarmDailyTetRemaining(context)
        }
        AlarmType.WIDGET_TICKING -> {
            createAlarmForWidgetTicking(context)
        }
    }

    alarmManager.cancel(pendingIntent)
}
