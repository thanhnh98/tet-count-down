package com.thanh_nguyen.test_count_down.sys_communicate.service

import android.content.Intent
import com.thanh_nguyen.test_count_down.app.CountDownWidget
import com.thanh_nguyen.test_count_down.utils.isTetOnGoing
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay

class WidgetUpdateService: BaseService() {
    override fun onCreate() {
        super.onCreate()
        val currentService = Intent(this, WidgetUpdateService::class.java)
        val widget = Intent(
            this,
            CountDownWidget::class.java
        )

        observeEvent {
            if (isTetOnGoing()) {
                with(Dispatchers.IO) {
                    try {
                        stopService(currentService)
                    } catch (e: Exception) {
                    }
                }
            } else {
                with(Dispatchers.IO) {
                    while (true) {
                        try {
                            sendBroadcast(
                                widget
                            )
                        } catch (e: Exception) {
                            stopService(currentService)
                            e.printStackTrace()
                        }
                        delay(1000)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

    }
}