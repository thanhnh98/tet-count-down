package com.thanh_nguyen.test_count_down.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Base64
import androidx.core.content.ContentProviderCompat.requireContext
import com.thanh_nguyen.test_count_down.App
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream

suspend fun encodeToBase64(image: Bitmap?, quality: Int  = 100): String? {
    if (image == null)
        return null
    val base64: Deferred<String?>
    coroutineScope {
        base64 = async {
            withContext(Dispatchers.IO){
                val bm: Bitmap = image
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.PNG, quality, baos)
                val b: ByteArray = baos.toByteArray()
                Base64.encodeToString(b, Base64.DEFAULT)
            }
        }
    }
    return base64.await()
}

suspend fun decodeBase64(input: String?): Bitmap? {
    if (input.isNullOrEmpty())
        return  null

    val bitmap: Deferred<Bitmap?>
    coroutineScope {
        bitmap = async {
            withContext(Dispatchers.IO){
                val decodedByte: ByteArray = Base64.decode(input, 0)
                BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.size)
            }
        }
    }
    return bitmap.await()
}

suspend fun  Uri.toBimap(): Bitmap? {
    val bitmap: Deferred<Bitmap?>
    coroutineScope {
        bitmap = async {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                ImageDecoder.decodeBitmap(ImageDecoder.createSource(App.getInstance().contentResolver, this@toBimap))
            } else {

                MediaStore.Images.Media.getBitmap(App.getInstance().contentResolver, this@toBimap)
            }
        }
    }
    return bitmap.await()
}