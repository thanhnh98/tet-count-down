package com.thanh_nguyen.test_count_down.utils

import android.net.Uri
import com.extension.image_compressor.ImageCompressor
import com.thanh_nguyen.test_count_down.App
import com.thanh_nguyen.utils.WTF
import java.io.File


suspend fun compress(uri: Uri): File? {
    return try {
        ImageCompressor.compress(App.getInstance(), uri)
    }
    catch (e: Exception){
        e.printStackTrace()
        null
    }
}

val File.size get() = if (!exists()) 0.0 else length().toDouble()
val File.sizeInKb get() = size / 1024
val File.sizeInMb get() = sizeInKb / 1024
val File.sizeInGb get() = sizeInMb / 1024
val File.sizeInTb get() = sizeInGb / 1024