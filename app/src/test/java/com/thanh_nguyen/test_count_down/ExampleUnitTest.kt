package com.thanh_nguyen.test_count_down

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun `thanh ne`(){
        val fruits = listOf("cherry", "blueberry", "citrus", "apple", "apricot", "banana", "coconut")

        val evenFruits = fruits.groupingBy { it.first() }
            .fold(
                { key, value ->
                    value
                },
                { _, accumulator, element ->
                    accumulator.also {
                        println("dât ne: ${it}")
                    }
                }
            )

//        val sorted = evenFruits.values.sortedBy { it.first }
        println(evenFruits.values.toString()) // [(a, []), (b, [banana]), (c, [cherry, citrus])]
    }

    @Test
    fun testAsync(){
        runBlocking {
            funcDelay(2000, "1")
        }

        runBlocking {
            funcDelay(1000, "2")

        }

        runBlocking {
            funcDelay(3000, "3")
        }

        println("end")

    }

    suspend fun funcDelay(time: Long, text: String){
        delay(time)
        println(text)
    }
}